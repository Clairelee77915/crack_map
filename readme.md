### S3 Information
Bucket: lum-wintcm

Region: us-west-2

Text Path: /CrackMap/Textfile/

Map Result Path: /CrackMap/Output/

Input Path: /CrackMap/Input/

### Project Information
Script Location: taiappetl01:/opt/crack_map

Input Location: taiappetl01:/opt/crack_map/Crack

Map Result Location: taiappetl01:/home/nifi/crack_map/Result

Text Location: taiappetl01:/home/nifi/crack_map/Textfile