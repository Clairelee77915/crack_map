#!/usr/bin/env python2.7
import pandas as pd
import os, sys, datetime, logging

logger = logging.getLogger('crack_map')
logger.setLevel(logging.INFO)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.INFO)

logger.addHandler(consoleHandler)

formatter = logging.Formatter('%(asctime)s  %(name)s  %(levelname)s: [%(lineno)d] %(message)s')
consoleHandler.setFormatter(formatter)


if (len(sys.argv) < 1):
    print("Usage: crack_v5.py <percantage>")
    quit()
else:
    percantage = sys.argv[1]

cwd = os.getcwd()
pathresult = '/home/nifi/crack_map/Result'
#pathresult = os.path.join(cwd, 'Result')
#pathtext = os.path.join(cwd, 'Textfile')
pathtext = '/home/nifi/crack_map/Textfile'
pathcrack = os.path.join(cwd, 'Source', 'Crack')
file_name = 'KRK_GGJ_MP_WM_WD057_{}_032820'.format(percantage)
filemap = '{}.csv'.format(os.path.join(cwd, 'Source', 'Map', file_name))

foldercrack = os.listdir(pathcrack)
folderresult = os.listdir(pathresult)

# Filter mapped wafers
filtered_file = [file for file in foldercrack if file.replace('.xls', '') not in folderresult]
looptotal = len(filtered_file)
loopindex = 0.0
for filecrack in filtered_file:
    if 'xls' in filecrack:
        data = pd.read_excel(os.path.join(pathcrack, filecrack), sheet_name='whole',  usecols=['X', 'Y', 'inspection'])
        WaferID = filecrack[:-4]
    else:
        try:
            crack_file_name = os.listdir(os.path.join(pathcrack, filecrack))
            if len(crack_file_name) > 1:
                logger.error("Over than one crack map: {}".format(crack_file_name))
            data = pd.read_excel(os.path.join(pathcrack, filecrack, crack_file_name[0]), sheet_name='whole', usecols=['X', 'Y', 'inspection'])
            WaferID = filecrack
        except Exception as e:
            logger.warning("Skip error files, {}: {}".format(e, filecrack))
            continue
    logger.info("Start to Process: {}".format(WaferID))
    dataout = pd.read_csv(filemap, skiprows=2)
    merged_df = pd.merge(dataout, data, on=["X", 'Y'])

    fail_df = merged_df[['X', 'Y']][(merged_df['inspection'] == 'fail') | (merged_df['inspection'] == 'Fail')]
    for row in fail_df.iterrows():
        for xi in range(-4, 5):
            for yi in range(-4, 5):
                temp_df = merged_df[['X', 'Y', 'Contact Number']][(merged_df['X'] == row[1][0] + xi) & (merged_df['Y'] == row[1][1] + yi)]
                if temp_df.shape[0] == 1:
                    contact_number = temp_df.loc[temp_df.index[-1], 'Contact Number']
                    temp_df['Main Die'] = -997
                    merged_df.update(temp_df)
                    if contact_number > 0:
                        temp_df2 = merged_df[['X', 'Y']][merged_df['Contact Number'] == contact_number]
                        temp_df2['Main Die'] = -997
                        merged_df.update(temp_df2)
    merged_df['X'] = pd.to_numeric(merged_df['X'])
    merged_df['Y'] = pd.to_numeric(merged_df['Y'])

    header = pd.read_csv(filemap, nrows=2)
    columns = [u'Die Size', u'Die 1 X', u'Die 1 Y', u'Die 2 X', u'Die 2 Y', u'Die 3 X', u'Die 3 Y', u'Die 4 X', u'Die 4 Y'] + [""] * 12
    header['inspection'] = None
    header.at[1, 'inspection'] = 'inspection'
    merged_df.columns = columns
    header.columns = columns

    # generate map
    result = pd.concat([header, merged_df], ignore_index=True)
    outdir = os.path.join(pathresult, WaferID)
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    result.to_csv('{}.csv'.format(os.path.join(outdir, file_name)), index=False)
    now_date = datetime.datetime.now()

    # generate log files
    log_file = os.path.join(pathresult, 'LogCombine_{}.csv'.format(now_date.strftime('%Y-%m')))
    if os.path.isfile(log_file):
        log = pd.read_csv(log_file)
    else:
        log = pd.DataFrame()
    upload_date = datetime.datetime.fromtimestamp(os.stat(pathcrack + '/' + filecrack).st_ctime)
    log = log.append({'Lot_ID': WaferID,
                      'Win upload date': upload_date,
                      'Percentage': percantage,
                      'Map created time': now_date}, ignore_index=True)
    logger.info("\nLot ID: {} \nPercantage: {} \nWin upload date: {} \nMap created time: {}\n".format(
        WaferID, percantage, upload_date, now_date))
    log.to_csv(log_file, index=False)

    # generate text files
    df_txt = pd.DataFrame([{"Wafer": WaferID, "Tester(LNT+ '-' + Tester)": os.environ.get('tester', 'LNT-03')}],
                          columns=["Wafer", "Tester(LNT+ '-' + Tester)"])
    df_txt.to_csv(os.path.join(pathtext, '{}.txt'.format(WaferID)), index=False)
    loopindex = loopindex + 1
    logger.info("Progress: {} %".format(str(round(loopindex / looptotal * 100, 1))))

